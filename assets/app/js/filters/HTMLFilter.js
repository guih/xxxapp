

angular
    .module('Restaurant')
        .filter('html', HTMLFilter);

HTMLFilter.$inject = ['$sce'];

function HTMLFilter($sce){
    return function (text) {
        return $sce.trustAsHtml(text);
    }
}        