angular
    .module('Restaurant')
        .filter('trustUrl', URLFilter);

URLFilter.$inject = ['$sce'];

function URLFilter($sce){
    return function (Url) {
        return $sce.trustAsResourceUrl(Url);
    }
}        