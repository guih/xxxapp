angular
	.module('Restaurant')
	.controller('RestaurantsController', RestaurantsController);

RestaurantsController.$inject = [ 
		'$window'
	,	'$animate'
	,	'$location'
	,	 '$rootScope'
	,	 '$scope'
];

function RestaurantsController( $window,$animate,$location, $rootScope, $scope){

	var vm = this;
	$animate.enabled(false);
	$rootScope.$on("$locationChangeStart", function (event, next, current) {
		$animate.enabled(true);
    });
 


}