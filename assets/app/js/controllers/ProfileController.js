angular
	.module('Restaurant')
	.controller('ProfileController', ProfileController);

ProfileController.$inject = [ 
		'$window'
	,	'$animate'
	,	'$location'
	,	 '$rootScope'
	,	 '$scope'
];

function ProfileController( $window,$animate,$location, $rootScope, $scope){

    $scope.currentView  = null;
	var vm = this;
	$animate.enabled(false);
	$rootScope.$on("$locationChangeStart", function (event, next, current) {
		$animate.enabled(true);
    });
    


}