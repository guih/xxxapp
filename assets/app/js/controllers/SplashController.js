angular
	.module('Restaurant')
	.controller('SplashController', SplashController);

SplashController.$inject = [ 
		'$window'
	,	'$animate'
	,	'$location'
	,	 '$rootScope'
    ,	 '$scope'
    , '$timeout'
    , '$state'
];

function SplashController( $window,$animate,$location, $rootScope, $scope, $timeout, $state){

	var vm = this;
	$animate.enabled(false);
	$rootScope.$on("$locationChangeStart", function (event, next, current) {
        // console.log('PAN', arguments);
        // console.log($rootScope.routing)
        
		$animate.enabled(true);
    });

    $timeout(function(){
        $timeout(function(){
            $scope.loaded = true;            
            $scope.loadedView = true;
            // $rootScope.navigate('catalog');

        },1500)        
        $timeout(function(){
            $rootScope.navigate('catalog');
        },5000)
    },1000)
 

 


}