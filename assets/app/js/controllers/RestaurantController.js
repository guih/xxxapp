angular
	.module('Restaurant')
	.controller('RestaurantController', RestaurantController);

RestaurantController.$inject = [ 
		'$window'
	,	'$animate'
	,	'$location'
	,	 '$rootScope'
	,	 '$scope'
	, '$stateParams'
	, 'VideoService','$sce', 'DirectiveService', '$q', '$state',
];

function RestaurantController( $window,$animate,$location, $rootScope, $scope, $stateParams, VideoService, $sce, DirectiveService, $q, $state){
	var video = null;
	var likes = JSON.parse(localStorage.getItem("liked")) || [];
	var dislike = JSON.parse(localStorage.getItem("dislike")) || [];
	$scope.shouldSearch =  function(tag){
		// console.log('CURTRENT STATE', $state.current.name, $scope.searchField);
		// console.log('****************', tag, $state.current.name == 'restaurant' && $scope.searchField, $scope.searching);

		// if( tag == 'btn' ){
			var isDiff =  VideoService.searchCache.query != $scope.searchField;
			if( $state.current.name == 'catalog-search' || $state.current.name == 'catalog' ){
				if( isDiff && $scope.searchField != '' && $scope.searchField!= undefined){
					$scope.filterSearch($scope.searchField);
				}
				if( !$scope.searching )
					$scope.searching = !$scope.searching;			
			}
			if( ($state.current.name == 'catalog-search' || $state.current.name == 'restaurant') && $scope.searching == true ){
				console.log('?????????????', 'YESSSSSSSS', 'should redirect', $scope.searchField)

				if( $state.current.name == 'catalog-search' ){
					$scope.filterSearch($scope.searchField);

				}else{
					$state.go( 'catalog-search', { args : {query: $scope.searchField,  type: 'search' } } );					
				}
			}else{
				console.log('?????????????', 'YESSSSSSSS', 'ACTIVATED')

				$scope.searching = true;
			}		
		// }
		

	};

	$scope.searchT = function(ye){
		if( $scope.searching ){

			function loadData3(field){

				var deferred = $q.defer();
				setTimeout(function(){
					VideoService.searchVideos(field).then(function(result){
						console.log('fethtec')
					   deferred.resolve(result);
				   });
				}, 500);
			   return deferred.promise;
			}
		
			$scope.loading = true;
			// $scope.category = ye;
			$scope.videos = null;
			
			loadData3(ye).then(function(result){
				// $scope.$apply(function(){
					// alert('UE??')
					console.log('fethtec', result)

					$scope.needChange = true;
					var data= result.data;
					$scope.videos = null;

					$scope.videos = data.posts;
					VideoService.store.posts = $scope.videos;
					$rootScope.videos = data.posts;
					console.log('fethtec', $scope.videos)

					VideoService.store = data;
					$scope.cat = data.categories;
					$scope.genders = data.genders;
					$scope.loading = false;
				// });
			
			});	
					

		
			console.log('vai buscar...', $scope.searchField)
			
		}
		if( !$scope.searching ){
			$scope.searching = true;
			console.log('cleared....')
		}


	};
	$scope.$watch('searchField', function(){
		console.log(arguments)
	})
	var source = document.getElementById('video_target');
    $scope.currentView  = null;
	var vm = this;
	$scope.loading = true;
	console.log('>>>>>>>>>>>', $stateParams.videoId)
	$animate.enabled(false);
	$rootScope.$on("$locationChangeStart", function (event, next, current) {
	
		function loadData(id){
			$scope.loading = true;


			var deferred = $q.defer();
			setTimeout(function(){
				VideoService.getVideoById(id).then(function(resp){
				   deferred.resolve(resp);
			   	});
			}, 500);
		   return deferred.promise;
		}


		loadData($stateParams.videoId).then(function(resp){
		// $scope.$apply(function(){
			var resp = resp.data;
			$scope.video = resp.posts[0];
			

			console.log('YESSSSSSSS',resp.posts[0])			

			$scope.video.related = resp.related;
			if( likes.indexOf( $scope.video.id ) != -1 ){
				$scope.video.liked = true;
				
				
			}
			if( dislike.indexOf( $scope.video.id ) != -1 ){
				$scope.video.disliked = true;
			}			
			$scope.config = {
				preload: "none",
				sources: [
					{src: resp.posts[0].hotlink, type: "video/mp4"},
				],
	
				theme: {
					url: "https://unpkg.com/videogular@2.1.2/dist/themes/default/videogular.css"
				},
				poster: $scope.video.image
			};
			$scope.loading = false;
		
			// });
		
		});	
	
    });
    
    $scope.setView = function(view){
        $scope.currentView = view;
	}
	$scope.to_trusted = function(html_code) {
		return $sce.trustAsHtml(html_code);
	}

	$scope.like = function(id){
		var args = { action: 'like', id: id };
		VideoService.videoAction(args).then(function(res){
			
			if( res.data.caption == 'Success' ){
				likes.push(id); 
				localStorage.setItem("liked",  JSON.stringify(likes));
				$scope.video.likes++;
			}
		
			console.log('>>RES', res)
			if( likes.indexOf( $scope.video.id ) != -1 ){
				$scope.video.liked = true;

			}
			if( dislike.indexOf( $scope.video.id ) != -1 ){
				$scope.video.disliked = true;
			}					
		});
	}
	$scope.searchTag =  function(tag){
		$scope.videos = null;
		$rootScope.videos = null;
		// $scope.parentobj.videos = null;
		$state.go( 'catalog-listing', { args : {query: tag,  type: 'category' } } );
	};

	$scope.dislike = function(id){
		var args = { action: 'dislike', id: id };
		VideoService.videoAction(args).then(function(res){
			if( res.data.caption == 'Success' ){
				dislike.push(id); 
				localStorage.setItem("dislike",  JSON.stringify(dislike) );
				$scope.video.dislikes = parseInt($scope.video.dislikes) + 1 || 0;


			}			
			console.log('>>RES dislike', res)
			if( likes.indexOf( $scope.video.id ) != -1 ){
				$scope.video.liked = true;
			}
			if( dislike.indexOf( $scope.video.id ) != -1 ){
				$scope.video.disliked = true;
			}					
		});
	}


	// $rootScope.$on('$locationChangeSuccess', function() {
	// 	console.log*
	// });  
	$scope.reload = function(v){
		console.log('WHAT IS V?', v)

		DirectiveService.publish(v )


		// var setupOpt = {
		
		// 	// 'preload' : 'auto',
		// 	// 'poster' : asset.thumbnail,
	
		// 	"poster": v.image, 
		// 	sources: [ {type: "video/mp4", src: v.hotlink} ]
		// };
		
		// //inject $sce to use any url, or fetch url from http request
		// if( $rootScope.vid ){
		// 	$rootScope.vid.dispose();
		// 	// $rootScope.vid.pause();
		// }
	
		// videojs( 'video_holder', setupOpt, function(){
		
		// 	$rootScope.vid = videojs( 'video_holder' );
		//    $rootScope.vid.load();
		   
		// });
		
		// //destroy video when $rootScope is destroyed
		// $rootScope.$on( '$destroy', function() {
		// 	console.log( 'destroying video player' );
		// 	$rootScope.vid.dispose();
		// });



		

		// video = videojs("video_holder",{ 
		// 	"poster": v.image, 
		// 	sources: [{
		// 		src: v.hotlink,
		// 		type: 'video/mp4'
		// 	  }, {
		// 		src: v.hotlink,
		// 		type: 'video/webm'
		// 	}]
		// }, function(){
		// 	$scope.$on('$destroy', function () {
		// 		video.dispose();
		// 	});
		//   });
		// console.log(v);

		
		// document.getElementById('vjs-poster').setAttribute('style','style="background-image: url("'+v.image+'");"');
		// video.play();
			
	}
}