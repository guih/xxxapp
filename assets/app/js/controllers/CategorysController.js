angular
	.module('Restaurant')
	.controller('CategorysController', CategorysController);

CategorysController.$inject = [ 
		'$window'
	,	'$animate'
	,	'$location'
	,	 '$rootScope'
    ,	 '$scope'
    , 'VideoService'
    , '$q'
];

function CategorysController( $window,$animate,$location, $rootScope, $scope, VideoService, $q){

    $scope.currentView  = null;
	var vm = this;
	$animate.enabled(false);
	$rootScope.$on("$locationChangeStart", function (event, next, current) {
		function loadData(query){

			var deferred = $q.defer();
			setTimeout(function(){
                VideoService.fetchCategorys().then(function(result){                
				   deferred.resolve(result);
			   });
			}, 500);
		   return deferred.promise;
		}		
        // if( current.indexOf('#/categorys').length == -1 ){
            if( !$scope.categorys ){
                $scope.loading = true;
                loadData().then(function(categorys){
                    // $scope.$apply(function(){
                        var data = categorys.data;
                        $scope.categorys = data.posts;
                        $scope.loading = false;
                    // })
                    
                });
            }
            
        // }
   
		$animate.enabled(true);
    });
    


}