angular
	.module('Restaurant')
	.controller('MainController', MainController);

MainController.$inject = [ 
		'$window'
	,	'$animate'
	,	'$location'
	,	 '$rootScope'
	,	 '$scope',
	'$state',
	'VideoService','$timeout', '$stateParams', '$q'
];

function MainController( $window,$animate,$location, $rootScope, $scope, $state, VideoService, $timeout, $stateParams, $q){

	var vm = this;
	$animate.enabled(false);
	if( !VideoService.store.posts ){
		$scope.loading = true;

		VideoService.fetchVideos().then(function(result){
	
			$timeout(function(){
				$scope.needChange = true;
				var data= result.data;
				$scope.videos = data.posts;
				VideoService.store = data;
				$scope.cat = data.categories;
				$scope.genders = data.genders;
				$scope.loading = false;
			},1000)
		})
	}else{
		$timeout(function(){
			$scope.needChange = false;

			var data= VideoService.store;
			$scope.videos = data.posts;
			$scope.cat = data.categories;
			$scope.genders = data.genders;
			VideoService.store = data;

			$scope.loading = false;
		},1000)	
	}



	$rootScope.$on("$locationChangeStart", function (event, next, current) {
		var args = null;
		// if( $rootScope.routing ){
		// 	$rootScope.routing = next;
		// }
		// console.log('PAN', arguments);
		if( $stateParams.args ){
			args = $stateParams.args;
			if( args.type == 'search' ){
				$scope.searching = true;
				$scope.searchField = args.query;		
				$scope.filterSearch($scope.searchField);
			
			}

			if( args.type == 'all' ){
				
			}
			
			if( args.type == 'category' ){
				$scope.videos = null;
				$scope.searching = false;
				$scope.category = args.category;
				$scope.loading = true;
				$scope.filterCategory(args.cat);
				
			}
		}
		console.log($stateParams, $stateParams.args, '<>>><')
		
		// if( $stateParams.args.filter ){


	
		// }
		$animate.enabled(true);
	});
	$scope.filterSearch = function(query){
		function loadData(query){

			var deferred = $q.defer();
			setTimeout(function(){
			   VideoService.searchVideos(query).then(function(result){
				   deferred.resolve(result);
			   });
			}, 3000);
		   return deferred.promise;
		}		

		// VideoService.store.posts = null;

		$scope.loading = true;
		// $scope.category = query;
		// $scope.videos = null;
		loadData(query).then(function(result){
			// $scope.$apply(function(){
				$scope.needChange = true;
				var data= result.data;
				$scope.videos = data.posts;
				VideoService.store = data;
				$scope.cat = data.categories;
				$scope.genders = data.genders;
				$scope.loading = false;

			// });
		
		});	
	}

	$scope.loadMore = function(){
		if( !$scope.currentIndex ) $scope.currentIndex = 1;
		$scope.currentData = $scope.videos;
		$scope.currentIndex = $scope.currentIndex+1;

		function loadData(arg){
			$scope.loadingMore = true;
			var deferred = $q.defer();
			setTimeout(function(){
			   VideoService.getByIndex(arg).then(function(result){
				   deferred.resolve(result);
			   });
			}, 500);
		   return deferred.promise;
		}		

		var args = {
			paged:  $scope.currentIndex,
		};

		if( $scope.searchField ){
			args.search = $scope.searchField;
		}
		if( $scope.category ){
			args.cat = $scope.category;
		}
		

		loadData(args).then(function(result){
			// $scope.$apply(function(){
				$scope.needChange = true;
				var data= result.data;
				console.log('$scope.videos', $scope.videos)
				if( !data.posts){
					$scope.noPosts = true;
					return;
				}
				$scope.videos = $scope.videos.concat(data.posts);

					console.log('$scope.videos', $scope.videos)
				VideoService.store = data;
				$scope.cat = data.categories;
				$scope.genders = data.genders;
				$scope.loading = false;
				$scope.loadingMore = false;
				$scope.noPosts = false;

			// });
		
		});			
		
		
	};
	$scope.filterCategory = function(category){

		console.log('?????', category)
		
		
		function loadData(category){
			$scope.videos = [];
			$scope.loading = true;
			var deferred = $q.defer();
			setTimeout(function(){
			   VideoService.getByCategory(category).then(function(result){
				   deferred.resolve(result);
			   });
			}, 500);
		   return deferred.promise;
		}		
		function loadDatac(category){

			var deferred = $q.defer();
			setTimeout(function(){
				VideoService.fetchVideos().then(function(result){
				   deferred.resolve(result);
			   });
			}, 500);
		   return deferred.promise;
		}		
		
	
		console.log(category == $scope.category, category, $scope.category)
		if( category == $scope.category ){
			$scope.category = null;
			$scope.loading = true;
			$scope.searching = false;
			$scope.searchField = '';
			// // VideoService.store.posts = null;			
			loadDatac().then(function(result){
				// $scope.$apply(function(){
					$scope.needChange = true;
					var data= result.data;
					$scope.videos = data.posts;
					VideoService.store = data;
					$scope.cat = data.categories;
					$scope.genders = data.genders;
					$scope.loading = false;
				// });
			
			});	
		}else{
			$scope.loading = true;
			$scope.category = category;
			// $scope.videos = null;
			$scope.searching = false;
			$scope.searchField = '';
			// VideoService.store.posts = null;
			loadData(category).then(function(result){
				// $scope.$apply(function(){
					$scope.needChange = true;
					var data= result.data;
					$scope.videos = data.posts;
					VideoService.store = data;
					$scope.cat = data.categories;
					$scope.genders = data.genders;
					$scope.loading = false;
				// });
			
			});	
		}	

	}

	$scope.ToggleMenu = function(){
		$scope.displayMenu = !$scope.displayMenu;
	}

    $rootScope.navigate = function(path, ye){

			console.log(path, ye);
		if( ye ){
	
			$state.go(path, ye);

			
			// return;
		}else{
			$state.go(path);
		}

			console.log('')
		switch( path){
			case 'categorys':
				$rootScope.routing = 'catalog';
				$rootScope.routeName = 'Assistir';
				$rootScope.shouldBack = true;
				break;
			case 'catalog-listing':
				$rootScope.routing = 'catalog';
				$rootScope.routeName = 'Assistir';
				$rootScope.shouldBack = true;
			break;
			case 'restaurant':
				$rootScope.routing = 'catalog';
				$rootScope.routeName = 'Assistir';
				$rootScope.shouldBack = true;
			break;
		
			case 'catalog':
				$rootScope.routing = '';
				$rootScope.routeName = '';
				$rootScope.shouldBack = false;
				$scope.loading = true;

				VideoService.fetchVideos().then(function(result){
			
					$timeout(function(){
						$scope.needChange = true;
						var data= result.data;
						$scope.category = null;
						$scope.videos = data.posts;
						VideoService.store = data;
						$scope.cat = data.categories;
						$scope.genders = data.genders;
						$scope.loading = false;
					},1000)
				})				
			break;			
		}

	}
	$scope.$watch('searchField', function(){
		console.log(arguments)
	})
	$scope.shouldSearch =  function(tag){
		// console.log('CURTRENT STATE', $state.current.name, $scope.searchField);
		// console.log('****************', tag, $state.current.name == 'restaurant' && $scope.searchField, $scope.searching);

		// if( tag == 'btn' ){
			var isDiff =  VideoService.searchCache.query != $scope.searchField;
			if( $state.current.name == 'catalog-search' || $state.current.name == 'catalog' ){
				if( isDiff && $scope.searchField != '' && $scope.searchField!= undefined){
					$scope.filterSearch($scope.searchField);
				}
				if( !$scope.searching )
					$scope.searching = !$scope.searching;			
			}
			if( ($state.current.name == 'catalog-search' || $state.current.name == 'restaurant') && $scope.searching == true ){
				console.log('?????????????', 'YESSSSSSSS', 'should redirect', $scope.searchField)

				if( $state.current.name == 'catalog-search' ){
					$scope.filterSearch($scope.searchField);

				}else{
					$state.go( 'catalog-search', { args : {query: $scope.searchField,  type: 'search' } } );					
				}
			}else{
				console.log('?????????????', 'YESSSSSSSS', 'ACTIVATED')

				$scope.searching = true;
			}		
		// }
		

	};

	$scope.searchT = function(ye){
		if( $scope.searching ){

			function loadData3(field){

				var deferred = $q.defer();
				setTimeout(function(){
					VideoService.searchVideos(field).then(function(result){
						console.log('fethtec')
					   deferred.resolve(result);
				   });
				}, 500);
			   return deferred.promise;
			}
		
			$scope.loading = true;
			// $scope.category = ye;
			$scope.videos = null;
			
			loadData3(ye).then(function(result){
				// $scope.$apply(function(){
					// alert('UE??')
					console.log('fethtec', result)

					$scope.needChange = true;
					var data= result.data;
					$scope.videos = null;

					$scope.videos = data.posts;
					VideoService.store.posts = $scope.videos;
					$rootScope.videos = data.posts;
					console.log('fethtec', $scope.videos)

					VideoService.store = data;
					$scope.cat = data.categories;
					$scope.genders = data.genders;
					$scope.loading = false;
				// });
			
			});	
					

		
			console.log('vai buscar...', $scope.searchField)
			
		}
		if( !$scope.searching ){
			$scope.searching = true;
			console.log('cleared....')
		}


	};

	$scope.onlyBack = function(){
		$scope.searching = false;
	};


}