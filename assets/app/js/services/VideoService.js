angular
    .module('Restaurant')
    	.factory('VideoService', VideoService);
    	
VideoService.$inject = ['$http'];

function VideoService($http){
	var videos = {
        API_URL: 'http://193.37.213.37/wp/app-bk/wp-admin/admin-ajax.php'
    };
    videos.searchCache = {};

    videos.store = {};
    videos.single = {};
	
	videos.fetchVideos = function(){
        return $http({ 
            url: videos.API_URL, 
            method: "GET",
            params: {action: 'videos'}  
		});
    }
    videos.fetchCategorys = function(){
        return $http({ 
            url: videos.API_URL, 
            method: "GET",
            params: {action: 'videos',categorys: true}  
		});
	}
	
	videos.getByCategory = function(category){
		return $http({ 
            url: videos.API_URL, 
            method: "GET",
            params: {action: 'videos', cat: category}  
		});
    };
    
    videos.getByIndex = function(args){
        var args = Object.assign({action: 'videos'} , args, {});
		return $http({ 
            url: videos.API_URL, 
            method: "GET",
            params:  args
		});
    };
    
    videos.searchVideos = function(query){
        videos.searchCache = {
            query: query,
            posts: []
        };
		return $http({ 
            url: videos.API_URL, 
            method: "GET",
            params: {action: 'videos', search: query}  
		});
    };
    
    videos.getVideoById = function(id){
		return $http({ 
            url: videos.API_URL, 
            method: "GET",
            params: {action: 'videos', fetch: true, id: id}  
		});
    };

    videos.videoAction = function(args){
        var args = btoa(JSON.stringify( args ));
		return $http({ 
            url: videos.API_URL, 
            method: "GET",
            params: {action: 'videos', args: args}  
		});
    };    
    
    videos.getStatic = function(){
        return videos.store || [];
    }
    videos.getStaticSingle = function(){
        return videos.store || [];
    }    


	return videos;
}