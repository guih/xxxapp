angular
	.module('Restaurant', ['ionic','ui.router', 'ngAnimate',"com.2fdevs.videogular",'ionicLazyLoad'])
	.config(['$stateProvider', '$urlRouterProvider',
	function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');

		
		$stateProvider.state({
				name: 'splash',
				url: '/',
				templateUrl: 'assets/app/templates/splash.html',
				controller:'SplashController'
	  		}).state({
				name: 'catalog',
				url: '/catalog',
				templateUrl: 'assets/app/templates/restaurants.html',
				controller:'MainController'
			  })
			  .state({
				name: 'categorys',
				url: '/categorys',
				params: {args: null},
				templateUrl: 'assets/app/templates/categorys.html',
				controller:'CategorysController'
			  })
			  .state({
				name: 'catalog-search',
				url: '/catalog/search/{args:json}',
				params: {args: null},
				templateUrl: 'assets/app/templates/restaurants.html',
				controller:'MainController'
			  })
			  .state({
				name: 'catalog-listing',
				url: '/catalog/filter/{args:json}',
				params: {args: null},
				templateUrl: 'assets/app/templates/restaurants.html',
				controller:'MainController'
		  	}).state({
				name: 'restaurants',
				controller:'RestaurantsController',

				url: '/restaurants',
				templateUrl: 'assets/app/templates/restaurants.html'
			}).state({
				name: 'profile',
				controller:'ProfileController',

				url: '/profile',
				templateUrl: 'assets/app/templates/profile.html'
			}).state({
				name: 'restaurant',
				controller:'RestaurantController',
				url: 'restaurant/:videoId',
			    params: {
					videoId: null
				},
				templateUrl: 'assets/app/templates/restaurant.html'
		  });			
	}]);