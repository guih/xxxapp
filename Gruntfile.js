module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        path: 'assets/app/js',
        cachebreaker: {
            dev: {
                options: {
                    match: ['restaurant.js', 'style.css'],
                    // replacement: 'time'
                },
                files: {
                    src: ['index.html']
                }
            }
        },        
        concat: {
            js: {
                src: [
                    '<%= path %>/vendor/ionic.js',
                    '<%= path %>/vendor/*.js',
                    '<%= path %>/app/App.js', 
                    '<%= path %>/app/routes.js', 
                    '<%= path %>/services/*.js',
                    '<%= path %>/filters/*.js',
                    '<%= path %>/directives/*.js',
                    '<%= path %>/controllers/*.js',
                ],
                dest: '<%= path %>/restaurant.js',
                options: {
                    separator: ';\n'
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> v<%= pkg.version %>, <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            js: {
                src: '<%= path %>/restaurant.js',
                dest: '<%= path %>/restaurant.min.js'
            }
        },
        watch: {
            js: {
                files: [
                    '<%= path %>/app/*.js',
                    '<%= path %>/controllers/*.js',
                    '<%= path %>/services/*.js',
                    '<%= path %>/filters/*.js',
                    '<%= path %>/vendor/*.js',
                    '<%= path %>/directives/*.js'


                ],
                tasks: ['default'],
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-cache-breaker');

    grunt.registerTask('default', ['concat', 'cachebreaker']);
};